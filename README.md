# Getting Started
- This is a project to create the Rudle (RoUtine Definiton LanguagE) programming language. There is currently no organized documentation, that is still TODO!

- To run tests, run `python3 tests/{test file name}` from the base directory

# Rudle Roadmap

## Initial Brainstorming

- Sequence diagrams
- Syntax examples
-

## Python prototype

- Lexer
  - Output: list of tokens
- Parser
  - Input: list of tokens
  - Output: AST
- Greenhouse
  - Input: AST
  - Output: Action Tree
- Interpreter
  - Executes action tree
- Requirements of the prototype
    - Create helpful error messages 
    - ???

## Unit tests
- Write unit tests to excercise the codebase
    - Doing this after to give greater flexibility when initially programming
- Write unit tests for the Rust implimentation

## Rust re-implimentation
- Recreate the python project in Rust

## V0.0.1
- Requirements
    - ???
