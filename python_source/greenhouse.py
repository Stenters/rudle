# User define modules
from interpreter_types.ast_types import *
from interpreter_types.action_types import *

"""
Why action tree? see: https://www.freecodecamp.org/news/the-programming-language-pipeline-91d3f449c919/
"""


def status_check():
    print("greenhouse is working!")


def execute(abstract_syntax_tree: ASTNode):
    node = ActionNode.generate_from_AST_node(abstract_syntax_tree)
    return node