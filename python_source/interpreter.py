import lexer
import parser
import greenhouse


def status_check():
    """
    Runs the status check on all included files, to prove importing worked
    """
    lexer.status_check()
    parser.status_check()
    greenhouse.status_check()
    print("interpreter is working!")

def execute(text_to_interpret):
    """
    Executes the interpretation of a given text
    """
    # The lexer converts raw text into a list of tokens, which is much more regularly formatted
    token_list = lexer.execute(text_to_interpret)

    # The parser converts the token list into an abstract syntax tree, which encodes the execution order of its nodes
    abstract_syntax_tree = parser.execute(token_list)

    # The greenhouse "grows" an action tree. It's very similar to the AST, but it includes more context
    action_tree = greenhouse.execute(abstract_syntax_tree)

    # When implemented, this will actually execute the action the program requested
    # print(f"--- Executing ---")
    stdout = action_tree.execute("")
    # print(f"--- End execution ---")

    # For now, return the output of each stage
    return [
        token_list,
        abstract_syntax_tree,
        action_tree,
        stdout]