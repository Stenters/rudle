# Python modules
from enum import Enum
from collections import deque

# User defined modules
from interpreter_types.lexical_types import *

class ASTNode():
    children: list # Of ASTNode


    def append_child(self, child):
        self.children.append(child)


    def expect(self, token: LexicalToken, token_type: LexicalType):
        # print(f"Expecting {token_type} from {token}")
        assert token.key is token_type, f"Unexpected token found. Expected {token_type}, but found {token.key}."


    def __init__(self, tokens: deque):
        self.children = []

        while len(tokens) > 0:
            match tokens[0].key:
                case LexicalType.RoutineDefinition:
                    routine = ASTRoutine(tokens)
                    self.children.append(routine)

                case _:
                    raise AssertionError(f"Invalid top level token: {tokens[0]}. Tokens remaining: {tokens}")


    def __repr__(self):
        return self.toString("")
    
    def toString(self, prefix):
        str_list = [f"> AST"]

        for child in self.children:
            str_list.append(child.toString(prefix = "--"))

        return "\n".join(str_list)



class ASTRoutine(ASTNode):
    name: str

    def __init__(self, tokens: deque):
        """
        Routine Definition
        Text: Name
        RoutineParameterOpen
        Text?: Parameter
        RoutineParameterClose
        PipelineOpen
        ASTCommand+: children
        PipelineClose

        """
        self.children = []
        self.parameters = []

        self.expect(tokens.popleft(), LexicalType.RoutineDefinition)
        
        assert tokens[0].key == LexicalType.Text, f"Routine name expected after routine definition! {tokens[0].key} found!\nTokens to parse: {tokens}"
        self.name = tokens.popleft().value

        self.expect(tokens.popleft(), LexicalType.RoutineParameterOpen)

        # Check for parameters
        if tokens[0].key == LexicalType.Text:
            self.parameters.append(ASTParameter(tokens))

        self.expect(tokens.popleft(), LexicalType.RoutineParameterClose)
        self.expect(tokens.popleft(), LexicalType.PipelineOpen)

        while (tokens[0].key != LexicalType.PipelineClose):
                command = ASTCommand(tokens)
                self.children.append(command)

        self.expect(tokens.popleft(), LexicalType.PipelineClose)


    def toString(self, prefix: str) -> str:
        str_list = [f"{prefix}> routine: {self.name}"]

        for child in self.children:
            str_list.append(child.toString(prefix + "--"))

        return "\n".join(str_list)



class ASTParameter(ASTNode):
    str_val: str
    text_val: LexicalToken


    def __init__(self, param: LexicalToken):
        """
        String: str_val
        | Text: text_val
        """
        self.children = [] # For parsing later, it's easier to always have children
        valid_types = [LexicalType.String, LexicalType.Text]

        match param.key:
            case LexicalType.String:
                self.str_val = param.value.replace('"','')
            case LexicalType.Text:
                self.text_val = param.value
            case _:
                raise AssertionError(f"Invalid ASTParameter found: {param}. Valid types are {valid_types}")


    def get_param(self):
        if hasattr(self, 'str_val'):
            return self.str_val
        elif hasattr(self, 'text_val'):
            return self.text_val
        else:
            raise AssertionError("Getting parameter of uninitialized ASTParameter!")


    def toString(self, prefix: str) -> str:
        return f"{prefix}> parameter: {self.get_param()}\n"



class ASTCommand(ASTNode):
    name: str


    def __init__(self, tokens: deque):
        """
        Text: name
        RoutineParameterOpen
        (
        String: children[i]
        RoutineParameterSeparator
        )*
        RoutineParameterClose
        """
        self.children = []

        assert tokens[0].key == LexicalType.Text, f"Command name expected in routine body! {tokens[0].key} found!\nTokens to parse: {tokens}"
        self.name = tokens.popleft().value

        self.expect(tokens.popleft(), LexicalType.RoutineParameterOpen)

        while (tokens[0].key != LexicalType.RoutineParameterClose):
            if len(self.children) > 0:
                self.expect(tokens.popleft(), LexicalType.RoutineParameterSeparator)

            self.children.append(ASTParameter(tokens.popleft()))

        self.expect(tokens.popleft(), LexicalType.RoutineParameterClose)


    def toString(self, prefix: str) -> str:
        str_list = [f"{prefix}> command: {self.name}"]

        for child in self.children:
            str_list.append(child.toString(prefix + "--"))

        return "\n".join(str_list)

