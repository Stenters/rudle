# Python modules
from enum import Enum
# from collections import deque

# User defined modules
from interpreter_types import ast_types
from interpreter_types import lexical_types

class ActionKeyword(Enum):
    print = 10

    def __str__(self):
        return self.name

class ActionNode():
    children: list

    def __init__(self):
        self.children = []
        self.root = None

    def execute(self, stdin):
        stdout = ""

        for child in self.children:
            if child is ActionCommand:
                stdout += child.execute(stdin)

        return stdout

    def execute_routine(self, routine_name: str):
        routine_found = False
        for child in self.children:
            if type(child) is ActionRoutine and child.name is routine_name:
                routine_found = True
                child.execute()
        
        assert routine_found, f"Failed to execute routine {routine_name} on node {type(self)}. Children are:\n{self.children}"

    def get_children_from_ast(self, ast: ast_types.ASTNode, root):
        for child in ast.children:

            match type(child):
                case ast_types.ASTRoutine:
                    self.children.append(ActionRoutine.generate_from_AST_node(child, self))

                case ast_types.ASTParameter:
                    if type(child.get_param()) is str:
                        self.children.append(ActionString.generate_from_AST_node(child, self))
                    else:
                        self.children.append(ActionVariable.generate_from_AST_node(child, self))

                case ast_types.ASTCommand:
                    self.children.append(ActionCommand.generate_from_AST_node(child, self))

                case _:
                    raise AssertionError(f"Unsupported AST node found: {child}")

    @staticmethod
    def generate_from_AST_node(ast: ast_types.ASTNode):
        node = ActionNode()
        node.get_children_from_ast(ast, root=node)
        return node

    def __repr__(self):
        return self.toString("")
    
    def toString(self, prefix):
        str_list = ["> Action Tree"]

        for child in self.children:
            str_list.append(child.toString(prefix = "--"))

        return "\n".join(str_list)

class ActionRoutine(ActionNode):
    name: str
    children: list

    def __init__(self, name):
        super().__init__()
        self.name = name

    @staticmethod
    def generate_from_AST_node(ast: ast_types.ASTNode, root):
        assert type(ast) is ast_types.ASTRoutine, f"Invalid ASTNode, trying to parse ActionRoutine \n{ast}"

        node = ActionRoutine(ast.name)
        node.root = root
        node.get_children_from_ast(ast, root)

        return node

    def toString(self, prefix: str) -> str:
        str_list = [f"{prefix}> routine: {self.name}"]

        for child in self.children:
            str_list.append(child.toString(prefix + "--"))

        return "\n".join(str_list)


class ActionString(ActionNode):
    str_val: str

    def __init__(self, str_val):
        super().__init__()
        self.str_val = str_val

    def execute(self, stdin):
        return self.str_val

    @staticmethod
    def generate_from_AST_node(ast: ast_types.ASTNode, root):
        assert type(ast) is ast_types.ASTParameter, f"Invalid ASTNode, trying to parse ActionString \n{ast}"
        assert type(ast.get_param()) is str, f"Invalid ASTNode, trying to parse ActionString \n{ast}"

        node = ActionString(ast.get_param())
        node.root = root
        return node

    def toString(self, prefix: str) -> str:
        return f"{prefix}> string: {self.str_val}"


# TODO: how to handle variables?
class ActionVariable(ActionNode):
    str_val: str

    def __init__(self, lex_val):
        super().__init__()
        self.str_val = lex_val.value

    def execute(self, stdin):
        return self.str_val, ""

    @staticmethod
    def generate_from_AST_node(ast: ast_types.ASTNode, root):
        assert type(ast) is ast_types.ASTParameter, f"Invalid ASTNode, trying to parse ActionVariable \n{ast}"
        assert type(ast.get_param()) is lexical_types.LexicalToken, f"Invalid ASTNode, trying to parse ActionVariable \n{ast}"

        node = ActionString(ast.get_param())
        node.root = root
        return node

    def toString(self, prefix: str) -> str:
        return f"{prefix}> variable: {self.str_val}"


class ActionCommand(ActionNode):
    keyword: ActionKeyword
    routine_name: str
    
    def __init__(self, command_name: str):
        super().__init__()
        match command_name:
            case "print":
                self.keyword = ActionKeyword.print
            case _:
                self.routine_name = command_name


    def execute(self, stdin):
        match self.keyword:
            case ActionKeyword.print:
                stdout = self.children_to_string(stdin) + "\n"
            case _:
                self.root.execute_routine(self.routine_name)

        return stdout


    def children_to_string(self, stdin):
        ret_val = ""

        for child in self.children:
            ret_val += child.execute(stdin)

        return ret_val

    @staticmethod
    def generate_from_AST_node(ast: ast_types.ASTNode, root):
        assert type(ast) is ast_types.ASTCommand, f"Failed to parse ActionCommand\n{ast}"

        node = ActionCommand(ast.name)
        node.root = root
        node.get_children_from_ast(ast, root)

        return node

    def toString(self, prefix: str) -> str:
        str_list = [f"{prefix}> command: {self.keyword}"]

        for child in self.children:
            str_list.append(child.toString(prefix + "--"))

        return "\n".join(str_list)

