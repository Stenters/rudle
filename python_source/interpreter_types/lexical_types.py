from enum import Enum

def status_check():
    print("lexical_types is working!")


class LexicalType(Enum):
    Whitespace = 10                  # \s|\n|\t
    Trigger = 20                     # cron
    TriggerSeparator = 30            # :
    PipelineOpen = 40                # {
    PipelineClose = 50               # }
    RoutineDefinition = 60           # routine
    RoutineParameterOpen = 70        # (
    RoutineParameterSeparator = 80   # ,
    RoutineParameterClose = 90       # )
    String = 100                      # ".*"
    Text = 110                       # .+

    def __str__(self):
        return self.name


class LexicalToken():
    key = ""
    value = ""

    def __init__(self,key,value):
        self.key = LexicalType[key]
        self.value = value

    def __repr__(self):
        return f"|'{self.key}': '{self.value}'|"


class ActionNode():
    key = ""
    value = ""
    children = []

    def __init__(self,key,value, children = []):
        self.key = key
        self.value = value
        self.children = children

    def __repr__(self):
        
        children_strings = []

        for child in self.children:
            children_strings.append("\n\t" + child.__repr__())
        
        return f"['{self.key}': '{self.value}']" + children_strings.join("")
