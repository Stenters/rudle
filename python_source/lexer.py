from interpreter_types.lexical_types import *
import re

token_patterns = {
    "Whitespace": re.compile("\\s|\\n|\\t"),
    "Trigger": re.compile("cron"),
    "TriggerSeparator": re.compile(":"),
    "PipelineOpen": re.compile("\\{"),
    "PipelineClose": re.compile("\\}"),
    "RoutineDefinition": re.compile("routine"),
    "RoutineParameterOpen": re.compile("\\("),
    "RoutineParameterSeparator": re.compile("\\,"),
    "RoutineParameterClose": re.compile("\\)"),
    "String": re.compile("\"[^\"]*\""),
    "Text": re.compile("[\\w\\-]+")
}

def status_check():
    status_check()
    print("lexer is working!")


def execute(text_to_interpret, is_sub_execute=False):
    """
    The Lexer's job is to take the unformatted lines of the file to interpret, and output a list of tokens.
    The tokens outputted are an easier representation of the text to process.
    To accomplish the change, this lexer uses Regular Expressions to "bite off" chunks of the input text, 
        categorizing those chunks into tokens
    """

    # Initialize
    token_list = []
    index = 0

    # Iterate over string
    while index < len(text_to_interpret):
        tokens = get_next_token(index, text_to_interpret)

        if tokens == []:
            break

        # print(f"Token output: {tokens}")
        for token, size in tokens:

            if is_sub_execute:
                token_list.append((token,size))
            else:
                token_list.append(token)

            index += size

    return [token for token in token_list if token.key != LexicalType.Whitespace]

def get_next_token(index, text_to_interpret):
    short_text = text_to_interpret[index:]
    patterns = token_patterns.keys()
    token_key, token_value = "", ""
    sub_tokens = []

    if text_to_interpret == "":
        return []

    for pattern in patterns:
        match_result = token_patterns[pattern].match(short_text)

        if match_result:

            matched_text = match_result.group(0)

            if len(match_result.groups()) > 0:
                # Strip group 1 from group 0
                matched_text = matched_text.replace(match_result.group(1), "")
                # print(">>> " + matched_text)

                sub_tokens += execute(match_result.group(1),is_sub_execute=True)
                # print("Match result: " + match_result.group(1))
            # print("Match result: " + str(match_result))

            token_key = pattern
            token_value = matched_text

            break

    # if not token_key:
    #     print("empty key: '" + str(token_key) + "'")
    # if not token_value:
    #     print("empty value: '" + str(token_value) + "'")
    # print(f"Parsed key({token_key}) and value({token_value})")

    if len(token_key) > 0 and len(token_value) > 0:
        sub_tokens = [(LexicalToken(token_key,token_value), len(token_value))] + sub_tokens
        return sub_tokens
    else:
        print("Invalid text: '" + short_text + "'")
        return [(LexicalToken("error", "invalid"), 100)]
