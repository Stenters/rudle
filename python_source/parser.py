# Python defined modules
from collections import deque

# User defined modules
from interpreter_types.lexical_types import *
from interpreter_types.ast_types import *

def status_check():
    print("parser is working!")

# def expect(token_list: list[LexicalToken], index: int, token: LexicalToken):
#     assert token_list[index] == token, f"Expected {token}, got {token_list[index]}!"
#     return index + 1

# def parse_command(token_list: list[LexicalToken], index):
#     """
#     Method: Text
#     RoutineParameterOpen
#     Value: String
#     RoutineParameterClose
#     """

# def parse_routine(token_list: list[LexicalToken], index):
#     """
#     Name: Text 
#     PipelineOpen
#     Body: ASTCommand +
#     PipelineClose
#     """
#     sub_list = token_list[index:]
    
#     routine = ASTNode_Routine(sub_list[0])
#     index = expect(token_list, index, LexicalToken.PipelineOpen)

#     breakpoint()

#     return AST, index


# def is_top_level_token(token: LexicalToken):
#     # Currently the only valid top level token is a routine definition
#     return token.key is LexicalType.RoutineDefinition


def execute(token_list: list[LexicalToken]):
    """
    The parser takes the list of tokens and outputs an abstract syntax tree
    """
    token_queue = deque(token_list)
    AST = ASTNode(token_queue)

    return AST