# General routine
routine hello-world() {
    print("hello world!")
}

# Routine with multiple commands
routine hello-world() {
    print("hello world!")
    print("Bye")
}

# Routine with parameter
routine shout(text) {
    print(text)
}

# Routine with trigger
routine hello-at-midnight:
    cron 0 0 * * *
{
    print("hello!")
}



# Definiton
routine := "routine" name "(" parameter? (parameter_separator parameter)* ")" (":" trigger)? "{" command* "}"
name := text
parameter := text
parameter_separator = ","
trigger := "cron" ([0-9|*]){4}
command := "print(" (text|string) ")"
text := [a-zA-Z0-9\-\_]+
string := '"'[a-zA-Z0-9]*'"'