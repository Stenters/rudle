# Python modules
import os
import sys

# CD into python source
test_dir = os.path.dirname(__file__)
source_dir = os.path.join(test_dir, '..','python_source')
sys.path.append(source_dir)

# User defined modules
import interpreter

test_cases = [
    "sample-programs/hello-world.rdl", 
    "sample-programs/hello-and-bye.rdl",
    "sample-programs/hello-world-concat.rdl",
    "sample-programs/shout.rdl", # TODO: add parameterized routine support
    # "sample-programs/hello-at-midnight.rdl", # TODO: add cron support
    ]

def run_tests():
    """
    Attempts to interpret all the files in the `test_cases` list

    TODO: Actually check the output
    """
    
    file_text = ""

    # Run a test for every file defined above
    for test_file in test_cases:
        # Read the testfile
        with open(test_file) as file:
            file_text = file.read()

        # For the test file, interpret it
        result = interpreter.execute(file_text)

        # For debug, print values
#         print(f"File was: \n'{file_text.strip()}'")
#         print(f"""
# The output of the program was:
# ____________________________________________
#     Lexer:
# {result[0]}
# ____________________________________________
#     Parser:
# {result[1]}
# ____________________________________________
#     Greenhouse:
# {result[2]}
# ____________________________________________
#     Standard out:
# {result[3]}
# ____________________________________________
# """)
        print(f"""
    Standard output:
{result[3]}
""")


if __name__=="__main__":
    run_tests()